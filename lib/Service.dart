import 'dart:convert';

import 'package:forms_practice/models/usuario.dart';
import 'package:http/http.dart';
class Services {


  Future<List<Usuario>> insertUser(Map datos) async {
    String parsedJson = jsonEncode(datos["data"]);
    print(parsedJson);
    final res = await post(datos["url"],
        body: parsedJson
    );
    print(res.body);
    dynamic response = await this.getAllUsers();

    return response;

  }

  Future<List<Usuario>> getAllUsers() async {
    print("tomando usuarios");
     final res = await get("http://192.168.1.14:3000/users/all");
      print("==========================()=================================");
     //print(res.statusCode);
     return ParseUsuarios(res.body);
  }
List<Usuario> ParseUsuarios(String responseBody){
 final Parsed = json.decode(responseBody).cast<Map<String,dynamic>>();
 //return Parsed.map<Usuario>((json)=>Usuario.fromJson(json))
 final list = Parsed.map<Usuario>((json) => Usuario.fromJson(json)).toList();
 return list;
}

  Future<dynamic> getDatosFromServer(Map datos){

    switch (datos["method"]) {
      case "GET":
        switch(datos["route"]) {
          case "/users":
            return this.getAllUsers();
            break;
        }
        break;
      case "POST":
        switch(datos["route"]) {
          case "/users":
            return this.insertUser(datos);
            break;
        }
    }
    return Future.delayed(Duration(seconds: 2)).then((value) => true);
  }


}