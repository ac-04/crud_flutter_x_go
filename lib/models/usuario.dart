class Usuario{

  int userid;
  String name;
  String location;
  int age;

  Usuario({this.userid,this.name,this.location,this.age});

  factory Usuario.fromJson(Map<String,dynamic> json){

    return Usuario(
      userid: json["userid"] as int,
      name: json["name"] as String,
      location: json["location"] as String,
      age: json["age"] as int
    );

  }

}