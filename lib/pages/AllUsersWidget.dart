import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:forms_practice/models/usuario.dart';

import 'formularios/formularioAddUser.dart';
import 'mostrarUsuario.dart';

class allUsersWidget extends StatelessWidget {
  List<Usuario> usuarios = [];
  @override
  Widget build(BuildContext context) {
    usuarios = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("All users"),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            FlatButton.icon(
              color: Colors.green[600],
                textColor: Colors.white,
                onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FormularioAddUser(),
                  )
                );
                },
                icon: Icon(Icons.person_add,color: Colors.white,),
                label: Text("Agregar Usuario",)),
            const titulo(),
            Flexible(
              child: SizedBox.expand(child: listarUsuarios(usuarios: usuarios)),
            )
          ],
        ),
      ),
    );
  }
}

class listarUsuarios extends StatefulWidget {
  listarUsuarios({
    @required this.usuarios,
  });

  List<Usuario> usuarios;
  @override
  _listarUsuariosState createState() => _listarUsuariosState(usuarios:usuarios);
}

class _listarUsuariosState extends State<listarUsuarios> {
  _listarUsuariosState({
    @required this.usuarios,
  });

   List<Usuario> usuarios;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount:usuarios.length-1,
      itemBuilder:(context, index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            dense: true,
            title: Text("${usuarios[index].name}",style: TextStyle(fontWeight: FontWeight.bold),),
            subtitle: Text("edad: ${usuarios[index].age} \n pais: ${usuarios[index].location}"),
            onTap: (){
              _mostrarUnUsuario(context,usuarios[index]);
            },
            leading: Icon(Icons.person,size: 56.0,color: Colors.blue,),
            contentPadding: EdgeInsets.all(10.0),
          ),
        );
      },
    );
  }
}


_mostrarUnUsuario(BuildContext context,Usuario usuario) async {
  final result = await Navigator.push(
    context,
    MaterialPageRoute(builder: (context)=> MostrarUsuario(usuario))
  );
  print(result);
}


class titulo extends StatelessWidget {
  const titulo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0,10.0,0,0),
      child: Container(
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.only(left: 10.0,right: 10.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black45,
            width: 2.0,
          ),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.blue, Colors.purple])
        ),
        child: Text(
          "Users",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            letterSpacing: 2.0,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}

