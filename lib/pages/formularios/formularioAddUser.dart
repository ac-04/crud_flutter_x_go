import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FormularioAddUser extends StatefulWidget {
  @override
  _FormularioAddUserState createState() => _FormularioAddUserState();
}

class _FormularioAddUserState extends State<FormularioAddUser> {

Map<String,dynamic> _campos = {
  "Name":"",
  "Age":1,
  "Location":""
};

Map<int,Map<String,dynamic>> camposMap = {
  0:{
    "icono":Icons.person,
    "titulo":"Nombre",
    "campo":"Name",
    "error":true
  },
  1:{
    "icono":Icons.timer,
    "titulo":"Edad",
    "campo":"Age",
    "error":true
  },
  2:{
    "icono":Icons.location_on,
    "titulo": "Pais",
    "campo":"Location",
    "error":true
  }
};

AllCamposAreSet(){
  bool internReturn = true;
  this.camposMap.forEach((key, campo) {
    if(campo["error"]){
      internReturn = false;
    }
  });
  return internReturn;
}

  List<Widget> x_fields(){
    return List.generate(3, (index) => Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextField(
        keyboardType: camposMap[index]["titulo"]=="Edad" ? TextInputType.number : TextInputType.text,
        inputFormatters:  camposMap[index]["titulo"]=="Edad" ?[WhitelistingTextInputFormatter.digitsOnly] : null,
      decoration: InputDecoration(
        errorBorder: const OutlineInputBorder(
          borderSide:const BorderSide(
            color: Colors.pink,
            width: 2.0
          )
        ),
        errorText: camposMap[index]["error"] ? "":null ,
          errorStyle: const TextStyle(
            fontSize: 20.0
          ),
          labelText: camposMap[index]["titulo"],
          prefixIcon: Icon(camposMap[index]["icono"],color: Colors.white,size: 50.0,),
          alignLabelWithHint: true,
          labelStyle: const TextStyle(
            color: Colors.white,
            fontSize: 19.0
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.white
            )
          )
        ),
        onChanged: (val){
          this._campos[camposMap[index]["campo"]]=val;
        if(val.isEmpty){
          if(!camposMap[index]["error"]){// SI ya esta en true no es necesario que vuelva a pintar todo de nuevo
            setState(() {
              camposMap[index]["error"] = true;
            });
          }
        }else{
          if(camposMap[index]["error"]){ // solamente si esta en true , vuelve a pintarse
            setState(() {
              camposMap[index]["error"] = false;
            });
          }
        }
        },
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    print("painting");
    return Scaffold(
      appBar: AppBar(
        title: Text("Agregar nuevo usuario"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            padding: EdgeInsets.fromLTRB(10,10,10,0),
            height: 700.0,
            width: double.maxFinite,
            child: Card(
              color: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)
              ),
              elevation: 5,
              child: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      height: 50.0,
                    ),
                    Text("Usuario",style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 5.0,
                      fontSize: 40.30,
                    ),),
                    SizedBox(
                      height: 20.0,
                    ),
                    ...x_fields(),
                    ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: [
                        FlatButton.icon(
                            onPressed: (){
                              if(this.AllCamposAreSet()){
                                Navigator.pushReplacementNamed(context, "/",arguments: {
                                  "url":"http://192.168.1.14:3000/users/",
                                  "method":"POST",
                                  "data":this._campos,
                                  "route":"/users"
                                });
                              }else{
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context){
                                    return AlertDialog(
                                      title: Text("Error"),
                                      content: Text("Campos faltan"),
                                     actions: [
                                       FlatButton(
                                         child: Text("Aceptar"),
                                         color: Colors.blueAccent,
                                         onPressed: (){
                                           Navigator.of(context).pop();
                                         },
                                       )
                                     ],
                                    );
                                  }
                                );
                              }
                            },
                            icon: Icon(Icons.save,color: Colors.white,),
                            label: const Text("Guardar",style: TextStyle(color: Colors.white,fontSize: 20.0),)),
                        FlatButton.icon(
                            onPressed: (){},
                            icon: Icon(Icons.close,color: Colors.red,),
                            label: const Text("Cancelar",style: TextStyle(color: Colors.red,fontSize: 20.0),))
                      ],
                    )
                  ],
                ),
              ),
            ),
    ),
        ),
      )
    );
  }
}
