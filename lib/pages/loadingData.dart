import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:forms_practice/Service.dart';

class LoadingData extends StatelessWidget {

  final spinkit = SpinKitFadingCube(
    size: 100.0,
    itemBuilder: (BuildContext context, int index) {
      return DecoratedBox(
        decoration: BoxDecoration(
          color: index.isEven ? Colors.blue : Colors.blueAccent,
        ),
      );
    },
  );

  @override
  Widget build(BuildContext context) {
    print("loading pintando de nuevo");
    Map datosForaneos =ModalRoute.of(context).settings.arguments;
    Map<String,dynamic> datos = {
      "url":"http://192.168.1.14:3000/users/all",
      "method":"GET",
      "data":null,
      "route":"/users"
    };
    if (datosForaneos != null){
      datos =datosForaneos;
    }
  Services service = Services();

    Future<List<dynamic>> FutureDatos = service.getDatosFromServer(datos);
    FutureDatos.then((value){
      Navigator.pushReplacementNamed(context, "/users",arguments: value);
    }).catchError((error, stackTrace){
      print(stackTrace);
      Navigator.pushNamed(context, "/error");
    });

    return Scaffold(
      appBar: AppBar(
         title: Text("Loading"),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            spinkit,
            Text("loading")
          ],
        ),
      )
    );
  }
}
