import 'package:flutter/material.dart';
import 'package:forms_practice/models/usuario.dart';
class MostrarUsuario extends StatefulWidget {
  Usuario usuario;
  MostrarUsuario(this.usuario);
  @override
  _MostrarUsuarioState createState() => _MostrarUsuarioState(usuario);
}

class _MostrarUsuarioState extends State<MostrarUsuario> {
  Usuario usuario;
  _MostrarUsuarioState(this.usuario);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("usuario seleccionado"),
      ),
      body: Center(
        child: Text("USUARIO SELECCIONADO ${usuario.name}"),
      ),
    );
  }
}
