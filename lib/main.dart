import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:forms_practice/pages/AllUsersWidget.dart';
import 'package:forms_practice/pages/errorDisplay.dart';
import 'package:forms_practice/pages/loadingData.dart';


void main()=>runApp(
    new MaterialApp(
      initialRoute: '/',
      routes: {
        '/':(context)=>LoadingData(),
        '/users':(context)=>allUsersWidget(),
        "/error":(context)=>ErrorDisplay()
      },
    )
);